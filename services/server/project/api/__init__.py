# project/api/__init__.py


from flask_restx import Api

from project.api.coronavirus import coronavirus_namespace
from project.api.ping import ping_namespace

api = Api(version="1.0", title="API", doc="/doc/")

api.add_namespace(ping_namespace, path="/api/ping")
api.add_namespace(coronavirus_namespace, path="/api/coronavirus")
