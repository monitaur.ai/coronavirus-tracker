# project/__init__.py


import os

from flask import Flask, redirect, request
from flask_cors import CORS
from werkzeug.contrib.fixers import ProxyFix

# instantiate the extensions
cors = CORS()


def create_app(script_info=None):

    # instantiate the app
    app = Flask(__name__)
    app.wsgi_app = ProxyFix(app.wsgi_app)

    # set config
    app_settings = os.getenv("APP_SETTINGS")
    app.config.from_object(app_settings)

    # set up extensions
    cors.init_app(app, resources={r"*": {"origins": "*"}})

    # register blueprints
    # from project.client.views import core_blueprint

    # app.register_blueprint(core_blueprint)

    # register api
    from project.api import api

    api.init_app(app)

    # shell context for flask cli
    @app.shell_context_processor
    def ctx():
        return {"app": app}

    @app.before_request
    def force_https():
        if (
            app.env != "development"
            and request.endpoint in app.view_functions
            and request.headers.get("X-Forwarded-Proto") == "http"
        ):
            url = request.url.replace("http://", "https://")
            code = 301
            return redirect(url, code=code)

    return app
