import Vue from 'vue'
import App from './App.vue'

import moment from 'moment';
import VueMoment from 'vue-moment';
import Buefy from "buefy";
import "buefy/dist/buefy.css";

Vue.use(Buefy, {defaultIconPack: 'fa'});

import VueApexCharts from "vue-apexcharts";
Vue.use(VueApexCharts);

Vue.component("apexchart", VueApexCharts);

Vue.use(VueMoment, {moment});

Vue.config.productionTip = false

new Vue({
  render: h => h(App),
}).$mount('#app')
